# 启动前端工程

1.安装依赖

```shell
npm install
```

2.启动工程

```shell
npm run serve
```

3.访问项目

localhost:8080

用户名：admin   

密码：123456



# 启动API接口

0.idea打开工程。

1.创建数据库test，编码：UTF-8，执行【api接口】文件夹下的test.sql。

2.修改application.yml中数据库密码

3.启动项目，默认端口9001，注意：

>  如果修改的话也要同步修改前端工程中的main.js中API地址！！！



# 打赏作者

![输入图片说明](https://images.gitee.com/uploads/images/2021/1025/234349_3120dfb0_1505145.png "payme.png")
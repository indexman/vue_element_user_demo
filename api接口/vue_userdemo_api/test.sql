/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50723
Source Host           : localhost:3306
Source Database       : test

Target Server Type    : MYSQL
Target Server Version : 50723
File Encoding         : 65001

Date: 2021-01-11 18:43:13
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `username` varchar(100) DEFAULT NULL COMMENT '用户名',
  `password` varchar(100) DEFAULT NULL COMMENT '密码',
  `email` varchar(100) DEFAULT NULL COMMENT '邮箱',
  `mobile` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'laoxu', 'abc123', 'indexman@126.com', '12312312');
INSERT INTO `tb_user` VALUES ('2', 'admin', '123456', 'admin@1.com', '1231234');
INSERT INTO `tb_user` VALUES ('3', 'admin3', '123456', 'admin@1.com', '1231234');
INSERT INTO `tb_user` VALUES ('4', 'admin4', '123456', 'admin@1.com', '1231234');
INSERT INTO `tb_user` VALUES ('5', 'admin5', '123456', 'admin@1.com', '1231234');
INSERT INTO `tb_user` VALUES ('6', 'admin6', '123456', 'admin@1.com', '1231234');
INSERT INTO `tb_user` VALUES ('7', 'admin7', '123456', 'admin@1.com', '1231234');
INSERT INTO `tb_user` VALUES ('8', 'admin8', '123456', 'admin@1.com', '1231234');
INSERT INTO `tb_user` VALUES ('9', 'admin9', '123456', 'admin@1.com', '1231234');
INSERT INTO `tb_user` VALUES ('10', 'admin10', '123456', 'admin@1.com', '1231234');
INSERT INTO `tb_user` VALUES ('11', 'admin11', '123456', 'admin@1.com', '1231234');
INSERT INTO `tb_user` VALUES ('12', 'admin12', '123456', 'admin@1.com', '1231234');

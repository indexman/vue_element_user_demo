package com.laoxu.java.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.laoxu.java.entity.User;
import com.laoxu.java.mapper.UserMapper;
import com.laoxu.java.service.UserService;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
}

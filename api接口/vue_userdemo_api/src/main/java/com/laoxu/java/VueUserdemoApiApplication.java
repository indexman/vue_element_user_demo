package com.laoxu.java;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.laoxu.java.mapper")
public class VueUserdemoApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(VueUserdemoApiApplication.class, args);
    }

}

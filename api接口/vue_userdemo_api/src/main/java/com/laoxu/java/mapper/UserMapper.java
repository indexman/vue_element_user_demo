package com.laoxu.java.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.laoxu.java.entity.User;


public interface UserMapper extends BaseMapper<User> {

}

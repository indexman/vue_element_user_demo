package com.laoxu.java.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.laoxu.java.entity.User;


public interface UserService extends IService<User>{
}
